pipeline{

    agent { label 'maven'}
  
    parameters {
        string(name: 'GIT_SOURCE_URL', defaultValue: 'https://Vikzus@bitbucket.org/Vikzus/fuse-getting-started.git', description: 'GitHub URL')
        string(name: 'TEMPLATE_SOURCE', defaultValue: 'src/main/openshift/fgstemplate.yml', description: '')
        string(name: 'BUILD_NAME', defaultValue: 'helloworld-fis', description: '')
        string(name: 'TEMPLATE_NAME', defaultValue: 'helloworld-fis-template', description: '')
        string(name: 'ENVIRONMENT', defaultValue: 'dev', description: '')
    }

    stages{
		stage('Checkout Git Repo'){
			steps{
				//Checks out project from bitbucket
	   			git url: "${params.GIT_SOURCE_URL}", branch: "master", credentialsId: 'e425712b-f10b-44d6-a872-f5c6a38ef5aa'
			}
	  	}

		stage('Unit Test'){
			steps{
				script{
				    try {
				    	//Run maven test
				    	sh "mvn test"
				    }catch (Exception err) {
				        print err
				        print err.message
				        currentBuild.result = 'FAILURE'
				    }
				}
			}
		}

		stage('Build WAR'){
		    steps{
		    	script{
		    		try{
		    			//Build maven project
				    	sh "mvn -DskipTests clean install"
				    }catch(Exception Err){
				    	print err
				    	print err.message
				    	currentBuild.result = 'Failure'
				    }
				}
		    }
		}
	  
		stage('Create Objects'){
			steps{
				script{
					try{
						//Clean up existing objects		
						template_exists = sh(script: "oc get templates -o name -n ${ENVIRONMENT}", returnStdout: true ).trim()
						if(template_exists == "templates/helloworld-fis-template"){
							sh "oc delete template ${params.TEMPLATE_NAME} -n ${ENVIRONMENT}"
							sh "oc delete all -l template=${params.BUILD_NAME} -n ${ENVIRONMENT}"
							sh "oc create -f ${params.TEMPLATE_SOURCE} -n ${ENVIRONMENT}"
						}else{
						//Create template
					    sh "oc create -f ${params.TEMPLATE_SOURCE} -n ${ENVIRONMENT}"
						}
					}catch(Exception Err){
						print err
				    	print err.message
				    	currentBuild.result = 'Failure'
					}	
				}
			}
		}    	    
	  
	    stage('Build App'){
	      	steps{
	      		script{
		        	try{
		        		//Create app from template and start build
		        		sh "oc new-app --template=${params.TEMPLATE_NAME} -n ${ENVIRONMENT}"
		          		sh "oc start-build ${params.BUILD_NAME} -n ${ENVIRONMENT}"    
		        	}catch(Exception Err){
			      		print err
			      		print err.message
			      		currentBuild.result = 'Failure'
			      	}
	           	}
	        }   	
	   	}

// 	    stage('Validate Deployment') {
// 	        steps {
// 	            script {
// 					sh "oc describe builds/helloworld-fis-1 | grep Status | awk 'BEGIN {FS=" "}{print $2}"                
// 	            }
// 	        }
// 	    }
 	}   	
 }
