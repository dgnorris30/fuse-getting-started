
node("maven"){
  stage('Checkout Git Repo'){
    git url: "${params.GIT_SOURCE_URL}", branch: "dev", credentialsId: '1a12dfa4-7fc5-47a7-aa17-cc56572a41c7'
  }

  stage('Unit Test'){
    try {

     sh "mvn test"

    } catch (Exception err) {
        print err
        print err.message
        currentBuild.result = 'FAILURE'
     }
  }

  stage('Build WAR'){
    try{
      sh "mvn -DskipTests clean install"
    }catch(Exception Err){
      print err
      print err.message
      currentBuild.result = 'Failure'
    }
  }
}
/*  stage('Build Fuse Image'){
    try{
      dir('openshift'){

          openshift.withProject("${params.DEV_PROJECT}"){


            //validate that template exists
            if(!openshift.selector("${params.TEMPLATE_NAME}").exists()){

                sh "oc create -f ${params.TEMPLATE_FILENAME}"

            }
            }
        }
      }
    }catch(Exception err){
      print err
      print err.message
      currentBuild.result = 'FAILURE'
    }
*/
